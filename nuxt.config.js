
export default {
  mode: 'spa',
  /*
  ** Headers of the page
  ** title: process.env.npm_package_name
  ** content: process.env.npm_package_description
  */
  head: {
    title: 'NUXT Starter',
    meta: [
      { charset: 'utf-8' },
      { name: 'theme-color', content: '#FFFFFF' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'NUXT Starter' },
      { name: 'keywords', content: 'nuxt, starter' },
      { name: 'robots', content: 'index, follow, noodp' },
      { name: 'googlebot', content: 'index, follow' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'manifest', href: '/manifest.json' },
      { rel: 'shortcut icon', href: '/favicons/favicon.ico' },
      { rel: 'icon', sizes: '32x32', type: 'image/ico', href: '/favicons/favicon.ico' },
      { rel: 'icon', sizes: '48x48', type: 'image/png', href: '/favicons/favicon@xs.png' },
      { rel: 'icon', sizes: '96x96', type: 'image/png', href: '/favicons/favicon@sm.png' },
      { rel: 'icon', sizes: '144x144', type: 'image/png', href: '/favicons/favicon@md.png' },
      { rel: 'icon', sizes: '192x192', type: 'image/png', href: '/favicons/favicon@lg.png' },
      { rel: 'apple-touch-icon', href: '/favicons/touch-icon-iphone.png' },
      { rel: 'apple-touch-icon', sizes: '76x76', href: '/favicons/touch-icon-ipad.png' },
      { rel: 'apple-touch-icon', sizes: '120x120', href: '/favicons/touch-icon-iphone-retina.png' },
      { rel: 'apple-touch-icon', sizes: '152x152', href: '/favicons/touch-icon-ipad-retina.png' },
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    'bulma/css/bulma.min.css',
    'spinkit/spinkit.min.css',
    '@/assets/style/styleguides/main.scss',
    '@/assets/style/embed-font.scss',
    '@/assets/style/scaffolding.scss',
    '@/assets/style/global.scss',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/apiService',
    '~/plugins/vendors/vue-notification.js',
    '~/plugins/vendors/vue-preview.js',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  },
  env: {
    mode: 'dev',
    dev: {
      baseUrl: '1234',
    },
    staging: {
      baseUrl: '1234',
    },
    prod: {
      baseUrl: '1234',
    },
  }
}
