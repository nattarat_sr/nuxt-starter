import Table from './Table';
import TableHeader from './TableHeader';
import TableHeaderColumn from './TableHeaderColumn';
import TableBody from './TableBody';
import TableBodyColumn from './TableBodyColumn';
import TableRow from './TableRow';
import TableScroll from './TableScroll';
export default {
  Table,
  TableHeader,
  TableHeaderColumn,
  TableBody,
  TableBodyColumn,
  TableRow,
  TableScroll,
}
