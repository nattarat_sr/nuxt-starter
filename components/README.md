# Component snippets

> [1] v-style เมื่อ Render เป็น HTML จะเป็น inline style ดังนั้นให้ใช้กับดีไซน์ที่เป็น Static ไม่มีการเปลี่ยนแปลงค่าตามหน้าจอ โดยถ้ามีการเปลี่ยนแปลงค่าให้ไปใช้ className แทน

> [2] Component ที่มีหลาย State ให้ใช้ v-bind และส่งเป็น data มาเป็นชุดแทนการใส่เป็น Attr ลงบน Component tag

## Layout

``` bash
<Layout
  className=""
  v-bind=""
>
  ...
</Layout>
```

## Container

``` bash
<Container
  className=""
  v-bind=""
  isFluid
>
  ...
</Container>
```

## Section

``` bash
<Section
  className=""
  v-bind=""
  :width=10
  :widthUnit="'px'"
  :height=10
  :heightUnit="'px'"
  :padding=10
  :paddingUnit="'px'"
  :paddingTop=10
  :paddingRight=10
  :paddingBottom=10
  :paddingLeft=10
  :margin=10
  :marginUnit="'px'"
  :marginTop=10
  :marginRight=10
  :marginBottom=10
  :marginLeft=10
  :backgroundColor="'#CCCCCC'"
  :border="1px solid #000000"
  :borderUnit="'px'"
  :borderTop=10
  :borderRight=10
  :borderBottom=10
  :borderLeft=10
  :borderRadius=10
  :borderRadiusUnit="'px'"
  :boxShadow="'0 0 10px rgba(0, 0, 0, 0.25)'"
>
  ...
</Section>
```

## ImageRatio

``` bash
<ImageRatio
  className=""
  v-bind=""
  :src="'https://i.imgur.com/uNUbDaT.png'"
  :alt="'Image'"
  :top=10
  :topUnit="'px'"
  :left=10
  :leftUnit="'px'"
  :width=100
  :widthUnit="'px'"
  :height=57
  :heightUnit="'px'"
  :borderRadius=10
  :borderRadiusUnit="'px'"
>
  ...
</ImageRatio>
```

## Notification

``` bash
<Notification
  className=""
  v-bind=""
  isSuccess
  isError
  group=""
  duration=5000
>
  ...
</Notification>
```

## Spacing

``` bash
<Spacing
  className=""
  v-bind=""
  :height=10
  :heightUnit="'px'"
>
  ...
</Spacing>
```

## Grid

``` bash
<Grid
  className=""
  v-bind=""
  isGutter5
  isGutter10
  isGutter15
  isGutter20
  isGutter25
  isGutter30
  isGutterVertical5
  isGutterVertical10
  isGutterVertical15
  isGutterVertical20
  isGutterVertical25
  isGutterVertical30
  isCenter
  isRight
  isSpaceBetween
>
  <GridColumn
    className=""
    v-bind=""
    isColumn1
    isColumn2
    isColumn3
    isColumn4
    isColumn5
    isColumn6
    isColumn7
    isColumn8
    isColumn9
    isColumn10
    isColumn11
    isColumn12
    isGrow
    isNarrow
  >
    ...
  </GridColumn>
</Grid>
```

## Button

``` bash
<Button
  className=""
  v-bind=""
  isFluid
  isSmall
  isPrimary
  isSecondary
  isDisabled
  isShowIconFront
  isShowIconBack
  :srcIconFront="'/images/icons/ic-xxxxx.svg'"
  :srcIconBack="'/images/icons/ic-xxxxx.svg'"
  :altIcon="''"
  :width=100
  :iconTop=10
  :iconLeft=10
  :iconWidth=10
  :iconHeight=10
>
  ...
</Button>
```

## Input

``` bash
<Input
  className=""
  v-bind=""
  isFluid
  isShowIconFront
  isShowIconBack
  isClickableIconFront
  isClickableIconBack
  isSmall
  isError
  isDisabled
  :srcIconFront="'/images/icons/ic-xxxxx.svg'"
  :srcIconBack="'/images/icons/ic-xxxxx.svg'"
  :altIcon="''"
  :type="''"
  :placeholder="''"
  :name="''"
  :value="''"
  :width=200
  :iconTop=10
  :iconLeft=10
  :iconWidth=15
  :iconHeight=15
/>
```

## Textarea

``` bash
<Textarea
  className=""
  v-bind=""
  isFluid
  isShowIconFront
  isShowIconBack
  isClickableIconFront
  isClickableIconBack
  isError
  isDisabled
  :srcIconFront="'/images/icons/ic-xxxxx.svg'"
  :srcIconBack="'/images/icons/ic-xxxxx.svg'"
  :altIcon="''"
  :placeholder="''"
  :name="''"
  :value="''"
  :width=200
  :iconTop=10
  :iconLeft=10
  :iconWidth=15
  :iconHeight=15
/>
```

## Checkbox

``` bash
<Checkbox
  className=""
  v-bind=""
  isChecked
  isDisabled
  isShowLabel
  :label="''"
  :name="''"
  :value="''"
/>
```

## Radio

``` bash
<Radio
  className=""
  v-bind=""
  isChecked
  isDisabled
  isShowLabel
  :label="''"
  :name="''"
  :value="''"
/>
```

## UploadFile

``` bash
<UploadFile
  className=""
  v-bind=""
  isDisabled
  isMultiple
  isShowLabel
  :name="''"
  :value="''"
  :accept="''"
  :srcIcon="'/images/icons/ic-xxxxx.svg'"
  :altIcon="''"
  :label="''"
/>
```

## Loading

``` bash
<Loading
  className=""
  v-bind=""
  isPulse
  isCircleFade
  isFlow
  :sizePulse=28
  :sizeCircleFade=15
  :sizeFlow="'calc(30px * 1.3)'"
/>
```

## LoadingOverlay

``` bash
<LoadingOverlay
  className=""
  v-bind=""
  isPulse
  isCircleFade
  isFlow
/>
```

## Modal

``` bash
<Modal
  className=""
  v-bind=""
  isSmall
  isLarge
>
  ...
</Modal>
```

## ModalHeader

``` bash
<ModalHeader
  className=""
  v-bind=""
  isShowIcon
  :srcIcon="'/images/icons/ic-xxxxx.svg'"
  :altIcon="''"
  :iconTop=10
  :iconLeft=10
  :title="''"
/>
```

## ModalBody

``` bash
<ModalBody
  className=""
  v-bind=""
>
  ...
</ModalBody>
```

## ModalFooter

``` bash
<ModalFooter
  className=""
  v-bind=""
>
  ...
</ModalFooter>
```

## FormControl

``` bash
<FormControl
  className=""
  v-bind=""
  isHorizontal
  isVertical
  isError
  isShowMandatory
  isShowIcon
  isShowHint
  isShowMessage
  :label="''"
  :labelWidth=100
  :labelPaddingTop=8
  :srcIcon="'/images/icons/ic-vue.svg'"
  :hint="''"
  :hintPaddingTop=8
  :message="''"
>
  ...
</FormControl>
```

## Table

``` bash
<Table
  className=""
  v-bind=""
  isStriped
  isFluid
>
  ...
</Table>
```

## TableHeader

``` bash
<TableHeader
  className=""
  v-bind=""
>
  ...
</TableHeader>
```

## TableHeaderColumn

``` bash
<TableHeaderColumn
  className=""
  v-bind=""
>
  ...
</TableHeaderColumn>
```

## TableBody

``` bash
<TableBody
  className=""
  v-bind=""
>
  ...
</TableBody>
```

## TableBodyColumn

``` bash
<TableBodyColumn
  className=""
  v-bind=""
>
  ...
</TableBodyColumn>
```

## TableRow

``` bash
<TableRow
  className=""
  v-bind=""
>
  ...
</TableRow>
```

## TableScroll

``` bash
<TableScroll
  className=""
  v-bind=""
>
  ...
</TableScroll>
```

## Select

``` bash
<Select
  className=""
  v-bind=""
  isFluid
  isSearchable
  isShowLabel
  isDisabled
  :placeholder="''"
  :width=300
/>
```

## DatePickerCustom

``` bash
<DatePickerCustom
  className=""
  v-bind=""
  isFluid
  isInline
  :model=""
  :placeholder="''"
  :format="''"
/>
```

## DatePickerCustom

``` bash
<DatePickerCustom
  isFluid
  :height="''"
  :heightUnit="''"
/>
```
