import Modal from './Modal';
import ModalHeader from './ModalHeader';
import ModalBody from './ModalBody';
import ModalFooter from './ModalFooter';
export default {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
}
