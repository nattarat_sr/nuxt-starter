import Loading from './Loading';
import LoadingOverlay from './LoadingOverlay';
export default {
  Loading,
  LoadingOverlay,
}
