import Button from './Button';
import Checkbox from './Checkbox';
import ComponentTemplate from './ComponentTemplate';
import Container from './Container';
import DatePickerCustom from './DatePickerCustom';
import FormControl from './FormControl';
import Grid from './Grid';
import ImageRatio from './ImageRatio';
import Input from './Input';
import Layout from './Layout';
import Loading from './Loading';
import Modal from './Modal';
import Notification from './Notification';
import Radio from './Radio';
import Scrollbar from './Scrollbar';
import Section from './Section';
import Select from './Select';
import Spacing from './Spacing';
import Table from './Table';
import Textarea from './Textarea';
import UploadFile from './UploadFile';
import InputText from './InputText.vue';

export default {
  Button: Button.Button,
  Checkbox: Checkbox.Checkbox,
  ButtonText: Button.ButtonText,
  ComponentTemplate: ComponentTemplate.ComponentTemplate,
  Container: Container.Container,
  DatePickerCustom: DatePickerCustom.DatePickerCustom,
  FormControl: FormControl.FormControl,
  Grid: Grid.Grid,
  GridColumn: Grid.GridColumn,
  ImageRatio: ImageRatio.ImageRatio,
  Input: Input.Input,
  Layout: Layout.Layout,
  Loading: Loading.Loading,
  LoadingOverlay: Loading.LoadingOverlay,
  Modal: Modal.Modal,
  ModalHeader: Modal.ModalHeader,
  ModalBody: Modal.ModalBody,
  ModalFooter: Modal.ModalFooter,
  Notification: Notification.Notification,
  Radio: Radio.Radio,
  Scrollbar: Scrollbar.Scrollbar,
  Section: Section.Section,
  Select: Select.Select,
  Spacing: Spacing.Spacing,
  Table: Table.Table,
  TableHeader: Table.TableHeader,
  TableHeaderColumn: Table.TableHeaderColumn,
  TableBody: Table.TableBody,
  TableBodyColumn: Table.TableBodyColumn,
  TableRow: Table.TableRow,
  TableScroll: Table.TableScroll,
  Textarea: Textarea.Textarea,
  UploadFile: UploadFile.UploadFile,
  InputText,
}