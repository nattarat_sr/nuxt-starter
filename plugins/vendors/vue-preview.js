import Vue from 'vue'
import VuePreview from 'vue-preview'

// Options
// https://photoswipe.com/documentation/options.html

Vue.use(VuePreview, {
  bgOpacity: 0.95,
  // fullscreenEl: false,
  // captionEl: false,
  shareEl: false,
  closeOnScroll: false,
  showHideOpacity: true,
})
