import { userService } from './userService'

export default ({ app }, inject) => {
  inject('userService',  userService);
}