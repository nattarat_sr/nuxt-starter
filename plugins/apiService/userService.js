import { apiService } from './apiService';

export const userService = (configService = {}) => {
  console.log('configService', configService)
  const service = apiService(configService);
  return {
    test: (params) => {
      return service.get(params);
    }
  }
}